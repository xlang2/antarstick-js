from datetime import datetime
from enum import Enum

experiment_id = int
camera_id = int
stick_id = int
image_id = int
stick_image_id = int


class image_user_action(Enum):
    APPROVED = 1
    DISCARDED = 2


class Database():
    def __init__(self, path: str):
        pass

    def close(self):
        pass

    def create_experiment(self, location_name: str, year: int, start: datetime, end: datetime, latitude: float, longitude: float) -> experiment_id:
        pass

    def create_camera(self, experiment_id: experiment_id, camera_name: str) -> camera_id:
        pass

    def create_image(self, camera_id: camera_id, file_path: str, processed_state, datetime: datetime, snow_present: bool, is_day: bool, reciprocal_image_id: image_id, user_action):
        # TODO: define processed_state
        pass
