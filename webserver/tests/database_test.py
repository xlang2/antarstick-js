from database import Database
from unittest import TestCase


class TestDatabase(TestCase):
    def setUp(self):
        self.db = Database(':memory:')

    def tearDown(self):
        self.db.close()

    def test_create_experiment(self):
        self.assertEqual(1, 1)

    def test_create_camera(self):
        self.assertEqual(1, 1)
