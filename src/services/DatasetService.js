import axios from 'axios'

const URL = 'https://antarstick-data.herokuapp.com'
const BASE_URL = URL

const apiClient = axios.create({
  baseURL: BASE_URL,
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

let useMultipost = false
let multipostData = []

export default {
  xget(path) {
    return apiClient.get(path)
  },
  xpost(path, data) {
    if (useMultipost) {
      multipostData.push([path, data])
      return Promise.resolve('Success')
    } else {
      return apiClient.post(path, data)
    }
  },

  getMetadata() {
    return this.xget('/db/metadata')
  },
  // getStick(id) {
  //   return apiClient.get('/sticksData/' + id)
  // },
  // getData() {
  //   return apiClient.get('/data')
  // },
  getDataByCamera(cam) {
    return this.xget('/db/data/@' + cam)
  },
  commitChangesToDisk() {
    return this.xpost('/commit', {}, true)
  },
  checkForPendingChanges() {
    return this.xget('/probe/pending_changes')
  },
  setImageDateTime(cameraID, imageName, dateTimeString) {
    return this.xpost(
      '/db/data/@' + cameraID + '/' + imageName + '/dateTime',
      dateTimeString
    )
  },
  setTimeSpan(minDateTime, maxDateTime) {
    return Promise.all([
      this.xpost('/db/metadata/start', minDateTime),
      this.xpost('/db/metadata/end', maxDateTime)
    ])
  },
  setReciprocalImage(
    cameraID,
    imageName,
    recipCameraID,
    closestRecipImageName
  ) {
    return this.xpost(
      '/db/data/@' +
        cameraID +
        '/' +
        imageName +
        '/reciprocalImageName/' +
        recipCameraID,
      closestRecipImageName
    )
  },
  setStickSnowHeight(cameraID, imageName, stickID, snowHeight) {
    return this.xpost(
      '/db/data/@' +
        cameraID +
        '/' +
        imageName +
        '/sticks/' +
        stickID +
        '/snowHeight',
      snowHeight
    )
  },
  setStickTop(cameraID, imageName, stickID, point) {
    return this.xpost(
      '/db/data/@' + cameraID + '/' + imageName + '/sticks/' + stickID + '/top',
      point
    )
  },
  setStickBottom(cameraID, imageName, stickID, point) {
    return this.xpost(
      '/db/data/@' +
        cameraID +
        '/' +
        imageName +
        '/sticks/' +
        stickID +
        '/bottom',
      point
    )
  },
  setFlagImageApprovedByUser(cameraID, imageName, flagState) {
    return this.xpost(
      '/db/data/@' + cameraID + '/' + imageName + '/approvedByUser',
      flagState
    )
  },
  setFlagImageDiscardedByUser(cameraID, imageName, flagState) {
    return this.xpost(
      '/db/data/@' + cameraID + '/' + imageName + '/discardedByUser',
      flagState
    )
  },
  setFlagImageUsedForTraining(cameraID, imageName, flagState) {
    return this.xpost(
      '/db/data/@' + cameraID + '/' + imageName + '/usedForTraining',
      flagState
    )
  },
  setFlagStickCorrectedByUser(cameraID, imageName, stickID, flagState) {
    return this.xpost(
      '/db/data/@' +
        cameraID +
        '/' +
        imageName +
        '/sticks/' +
        stickID +
        '/correctedByUser',
      flagState
    )
  },
  setFlagStickDiscardedByUser(cameraID, imageName, stickID, flagState) {
    return this.xpost(
      '/db/data/@' +
        cameraID +
        '/' +
        imageName +
        '/sticks/' +
        stickID +
        '/discardedByUser',
      flagState
    )
  },
  // setFlagStickCreatedByUser(cameraID, imageName, stickID, flagState) {
  //   return this.xpost(
  //     '/db/data/@' +
  //       cameraID +
  //       '/' +
  //       imageName +
  //       '/sticks/' +
  //       stickID +
  //       '/createdByUser',
  //     flagState
  //   )
  // },
  setFlagStickVisible(cameraID, imageName, stickID, flagState) {
    return this.xpost(
      '/db/data/@' +
        cameraID +
        '/' +
        imageName +
        '/sticks/' +
        stickID +
        '/visible',
      flagState
    )
  },
  addStick(cameraID, imageName, stickID, stickData) {
    return this.xpost(
      '/db/data/@' + cameraID + '/' + imageName + '/sticks/' + stickID,
      stickData
    )
  },
  multipostBegin() {
    useMultipost = true
    return Promise.resolve('Success')
  },
  multipostEnd() {
    useMultipost = false
    if (multipostData.length > 0) {
      const result = this.xpost('/multipost', multipostData)
      multipostData = []
      return result
    }
    return Promise.resolve('Success')
  },
  getImagePath(cameraID, imageName) {
    return '/image/' + cameraID + '/' + imageName
  },
  getImageURL(cameraID, imageName) {
    return BASE_URL + this.getImagePath(cameraID, imageName)
  },
  loadImage(cameraID, imageName) {
    return this.xget(this.getImagePath(cameraID, imageName))
  }
}
