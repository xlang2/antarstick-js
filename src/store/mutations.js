import Vue from 'vue'

export default {
  ADD_STICKS(state, sticks) {
    Vue.set(state, 'sticks', sticks)
  },
  ADD_CAMERAS(state, cameras) {
    Vue.set(state, 'cameras', cameras)
  },
  ADD_DATA_BY_CAMERA(state, { data, cameraID }) {
    Vue.set(state.data, cameraID, data)
  },
  SET_IMAGE_CHRONOLOGY(state, { cameraID, chronology }) {
    Vue.set(state.chronology, cameraID, chronology)
  },
  SET_ACTIVE_POINT(state, { stickID, cameraID, imageNames }) {
    Vue.set(state, 'activePoint', { stickID, cameraID, imageNames })
  },
  SET_IMAGE_DATE_TIME(state, { cameraID, imageName, dateTimeString }) {
    state.data[cameraID][imageName].dateTime = dateTimeString
  },
  SET_TIMESPAN(state, { start, end }) {
    Vue.set(state, 'timespan', { start, end })
  },
  SET_RECIPROCAL_IMAGE(
    state,
    { cameraID, imageName, recipCameraID, closestRecipImageName }
  ) {
    state.data[cameraID][imageName].reciprocalImageName[
      recipCameraID
    ] = closestRecipImageName
  },
  SET_STICK_SNOW_HEIGHT(state, { cameraID, imageName, stickID, snowHeight }) {
    // Vue.set(
    //   state.data[cameraID][imageName].sticks[stickID],
    //   'snowHeight',
    //   snowHeight
    // )
    state.data[cameraID][imageName].sticks[stickID].snowHeight = snowHeight
  },
  SET_STICK_TOP(state, { cameraID, imageName, stickID, point }) {
    // Vue.set(state.data[cameraID][imageName].sticks[stickID], 'top', point)
    state.data[cameraID][imageName].sticks[stickID].top = point
  },
  SET_STICK_BOTTOM(state, { cameraID, imageName, stickID, point }) {
    // Vue.set(state.data[cameraID][imageName].sticks[stickID], 'bottom', point)
    state.data[cameraID][imageName].sticks[stickID].bottom = point
  },
  SET_FLAG_IMAGE_APPROVED_BY_USER(state, { cameraID, imageName, flagState }) {
    state.data[cameraID][imageName].approvedByUser = flagState
  },
  SET_FLAG_IMAGE_DISCARDED_BY_USER(state, { cameraID, imageName, flagState }) {
    state.data[cameraID][imageName].discardedByUser = flagState
  },
  SET_FLAG_IMAGE_USED_FOR_TRAINING(state, { cameraID, imageName, flagState }) {
    state.data[cameraID][imageName].usedForTraining = flagState
  },
  SET_FLAG_STICK_CORRECTED_BY_USER(
    state,
    { cameraID, imageName, stickID, flagState }
  ) {
    state.data[cameraID][imageName].sticks[stickID].correctedByUser = flagState
  },
  SET_FLAG_STICK_DISCARDED_BY_USER(
    state,
    { cameraID, imageName, stickID, flagState }
  ) {
    state.data[cameraID][imageName].sticks[stickID].discardedByUser = flagState
  },
  // SET_FLAG_STICK_CREATED_BY_USER(
  //   state,
  //   { cameraID, imageName, stickID, flagState }
  // ) {
  //   state.data[cameraID][imageName].sticks[stickID].createdByUser = flagState
  // },
  SET_FLAG_STICK_VISIBLE(state, { cameraID, imageName, stickID, flagState }) {
    state.data[cameraID][imageName].sticks[stickID].visible = flagState
  },
  SET_MAX_SNOW_HEIGHT(state, height) {
    Vue.set(state, 'maxSnowHeight', height)
  },
  SET_LOCATION(state, { name, latitude, longitude }) {
    Vue.set(state, 'location', { name, latitude, longitude })
  },
  SET_SUN_DATA(state, sunData) {
    Vue.set(state, 'sunData', sunData)
  },
  SET_SUN_IMAGE(state, sunImage) {
    Vue.set(state, 'sunImage', sunImage)
  },
  SET_CHART_X_SPAN(state, { start, end }) {
    state.chartXSpan = { start, end }
  },
  SET_SHOW_DAYS_GRADIENT(state, showDaysGradient) {
    state.controls.showDaysGradient = showDaysGradient
  },
  SET_SHOW_IMAGE_DIALOG(state, showImageDialog) {
    state.controls.showImageDialog = showImageDialog
    state.redrawTrigger = !state.redrawTrigger
  },
  SET_SHOW_FILTERS(state, showFilters) {
    state.controls.showFilters = showFilters
  },
  SET_SHOW_LEGEND(state, showLegend) {
    state.controls.showLegend = showLegend
  },
  SET_SHOW_HELP_DIALOG(state, showHelpDialog) {
    state.controls.showHelpDialog = showHelpDialog
  },
  SET_ZOOM_STATE(state, zoomState) {
    state.zoomState = zoomState
  },
  SET_STICK_STATUS_GUI(state, { statusGUI, stickID }) {
    state.sticks[stickID].statusGUI = statusGUI
  },
  SET_CAMERA_STATUS_GUI(state, { statusGUI, cameraID }) {
    state.cameras[cameraID].statusGUI = statusGUI
  },
  SET_FILTER(state, { filterKind, value }) {
    state.filters[filterKind] = value
  },
  UPDATE_SNOW_HEIGHT_AVERAGE(state, { cameraID, imageName, stickID, average }) {
    Vue.set(
      state.data[cameraID][imageName].sticks[stickID],
      'snowHeightAverage',
      average
    )
  },
  SET_SERVER_PENDING_CHANGES(state, value) {
    state.server.hasPendingChanges = value
  },
  SET_NUMBER_PENDING_POST_REQUESTS(state, value) {
    state.server.numPendingPostRequests = value
  }
  // addStick(state, { cameraID, imageName, stickID, stickData, webCmd }) {
  //   // _sendPostRequest(state, webCmd)
  //   state.data[cameraID][imageName].sticks[stickID] = stickData
  // }
}
