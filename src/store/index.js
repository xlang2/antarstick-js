import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: false,
  state: {
    activePoint: { stickID: null, cameraID: null, imageNames: null },
    // Format: imageNames: {cameraID: 'imageName', cameraID: 'imageName'...}
    sticks: {},
    //Format: stickID: {
    // x: number,
    // y: number,
    // positionMean: {
    //  cameraID: {
    //    top: [number, number],
    //    bottom: [number, number],
    //  }
    //}
    // statusGUI: 'full' | 'compact' | 'minimized',
    // lengthCm: number,
    // labels: {cameraID: 'label', cameraID: 'label'},
    // primaryCamera: 'cameraID'}
    cameras: {},
    // Format: cameraID: {x: number, y: number, statusGUI: 'full' | 'minimized'}
    timespan: { start: new Date(), end: new Date() },
    maxSnowHeight: 0,
    snowHeightAverageTimeWindow: 12, // in hours
    location: { latitude: 0, longitude: 0 },
    data: {},
    // Format: cameraID:{
    //   imageName: {
    //     processedState: number,  // -1: skipped, 0: unprocessed, 1: processed
    //     dateTime: 'ISO 8601 timestamp',
    //     imageQuality: number,
    //     weatherConditions: number, // 0: no snow, 1: snow present
    //     isDay: bool,
    //     reciprocalImageName: { cameraID: 'imageName' },
    //     approvedByUser: bool, // TODO: delete
    //     discardedByUser: bool,
    //     usedForTraining: bool,
    //     sticks: {
    //       stickID: {
    //         top: [number, number],
    //         bottom: [number, number],
    //         snowHeight: number,
    //         snowHeightAverage: number,
    //         visible: bool,
    //         correctedByUser: bool,
    //         discardedByUser: bool,
    //         createdByUser: bool // TODO: delete
    //       },
    //     }
    //   }
    // }
    chronology: {
      // cameraID: {
      //   order: [imageName1, imageName2, ...] // Image names ordered chronologically by dateTime
      //   lookup: {  // a map from image names to indices into the array 'order' above
      //     imageName1: index1,
      //     imageName2: index2,
      //     ...
      //   }
      // }
    },
    sunData: [],
    // Format: [DateObject, DateObject...]
    // DateObject: {
    //   sunUpNadir: Date,
    //   sunUpZenith: Date,
    //   nadir: Date,
    //   astronomicalDawn: Date,
    //   nauticalDawn: Date,
    //   civilDawn: Date,
    //   sunrise: Date,
    //   sunset: Date,
    //   civilDusk: Date,
    //   nauticalDusk: Date,
    //   astronomicalDusk: Date
    // }
    sunImage: '',
    sunColors: {
      night: '#cfcdfc',
      astronomicalTwilight: '#ecd7db',
      nauticalTwilight: '#f9e4d5',
      civilTwilight: '#fef1e0',
      day: '#fffff4'
    },
    controls: {
      showDaysGradient: true,
      showImageDialog: false,
      showFilters: false,
      showLegend: false,
      showHelpDialog: true
    },
    zoomState: {
      selection: [0, 1],
      zoomingID: null
      // TODO: Add functions to manipulate this object into the object itself.
    },
    redrawTrigger: false, // Value is not important, the change of state will trigger chart redrawing.
    server: {
      numPendingPostRequests: 0,
      hasPendingChanges: false
    },
    filters: {
      imageProcessed: 2,
      isDay: 2, // 0: night, 1: day, 2: all
      snowDetected: 2,
      stickFound: 2, // 0: not found, 1: found, 2: all
      minSnowHeight: 0,
      maxSnowHeight: 0,
      imageSticksEdited: 2,
      imageApproved: 2,
      imageDiscarded: 2,
      imageUsedForSvmTraining: 2
    }
  },
  mutations,
  actions,
  getters
})
