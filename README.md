# Antarstick

Welcome to Antarstick, the revision system for snow height data from
time-lapse cameras. The trail cameras are pointed to a location with
several snow height markers (or stakes) made from bamboo stalks. Every
stake is tracked individually, and its snow height is visualized in
several ways.

## Open Antarstick online

For the easiest access to Antarstick without hassle, just open [antarstick.herokuapp.com](https://antarstick.herokuapp.com/).

## Install and run Antarstick locally
- Download and instal npm package manager.
- Install project `npm install` from the `antarstick-js` folder.
- You can either use a demo dataset, that is available on a remote server, or you can run a server locally and use the full dataset. Both options are described further down.

### Run Antarstick using the online demo dataset
- Run Antarstick using the command `npm run serve`.
- Open Antarstick at the address [http://localhost:8080/](http://localhost:8080/).

### Run Antarstick using local server, with full dataset
- Download the full dataset [here](http://decibel.fi.muni.cz/antarstick/). (One full year at one location has over 5 GB, so make sure you have enough space.)
- Extract the `antarstick-data` folder NEXT TO `antarstic-js` repository.
- Go to `antarstick-js/src/services/DatasetService.js` and set the URL variable to `http://localhost:3000/`.
- Have Python installed or install it [here](https://www.python.org/)
- Using `pip`, the Python packaging manager, install `flask` and `flask_cors`.
- Run `npm run database_monolith_lake_2011` or `npm run database_keller_stream_2011`, depending on a location you want to inspect.
- Run Antarstick using the command `npm run serve`.
- Open Antarstick at the address [http://localhost:8080/](http://localhost:8080/).
